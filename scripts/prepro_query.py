"""
Copyright (c) Microsoft Corporation.
Licensed under the MIT license.

preprocess TVR/TVQA annotations into LMDB
"""
import argparse
import copy
import json
import os
from os.path import exists

from cytoolz import curry
from tqdm import tqdm
from transformers import RobertaTokenizer

from hero.data.utils import open_lmdb
from hero.utils.basic_utils import save_jsonl, save_json


@curry
def roberta_tokenize(tokenizer, text):
    if text.isupper():
        text = text.lower()
    words = tokenizer.tokenize(text)
    ids = tokenizer.convert_tokens_to_ids(words)
    return ids


def process_tvr(jsonl, db, tokenizer):
    id2len = {}
    query2video = {}  # not sure if useful
    query_data = []
    for line in tqdm(
            jsonl,
            desc='processing TVR with raw query text'):
        example = json.loads(line)
        query_data.append(copy.copy(example))
        id_ = example['desc_id']
        input_ids = tokenizer(example["desc"])
        if 'vid_name' in example:
            vid = example['vid_name']
        else:
            vid = None
        if 'ts' in example:
            target = example['ts']
        else:
            target = None
        if vid is not None:
            query2video[id_] = vid
            example['vid'] = vid
        id2len[id_] = len(input_ids)
        example['input_ids'] = input_ids
        example['target'] = target
        example['qid'] = str(id_)
        db[str(id_)] = example
    return id2len, query2video, query_data


def process_football_generative(jsonl, db, tokenizer, max_target_length):
    id2len = {}
    query2video = {}  # not sure if useful
    query_data = []
    tokenizer_max_len = tokenizer.args[0].max_len

    for line in tqdm(
            jsonl,
            desc='processing Football data'):
        example = json.loads(line)
        query_data.append(copy.copy(example))
        match_id = example['match_id']

        history = "[SOC]"

        for chunk in example['chunks']:
            chunk_id = f"{match_id}_s{chunk['start']}_e{chunk['end']}"

            input_ids = tokenizer(history)

            if len(input_ids) > tokenizer_max_len:
                input_ids = input_ids[-tokenizer_max_len:]

            if 'caption' in chunk:
                target = tokenizer(chunk['caption'])
                if len(target) > max_target_length:
                    target = target[:max_target_length]
            else:
                target = None

            query2video[chunk_id] = match_id
            example['vid'] = match_id

            id2len[chunk_id] = len(input_ids)
            example['input_ids'] = input_ids
            example['target'] = target
            example['qid'] = str(chunk_id)

            db[str(chunk_id)] = example

            history += " " + chunk['caption'] + " [SEP] "

    return id2len, query2video, query_data


def process_football_retrieval(jsonl, db, tokenizer, max_target_length, window_size=1):
    id2len = {}
    query2video = {}  # not sure if useful
    query_data = []
    tokenizer_max_len = tokenizer.args[0].model_max_length

    for line in tqdm(
            jsonl,
            desc='processing Football data'):
        example = json.loads(line)
        query_data.append(copy.copy(example))
        match_id = example['match_id']

        for chunk_index, chunk in enumerate(example['chunks']):
            chunk_id = f"{match_id}_s{chunk['start']}_e{chunk['end']}"

            if chunk_index > 0:
                history = example["chunks"][chunk_index - 1]["caption"]
            else:
                history = "start"

            input_ids = tokenizer(history)

            if len(input_ids) > tokenizer_max_len:
                input_ids = input_ids[-tokenizer_max_len:]

            if 'caption' in chunk:
                candidates = [chunk["caption"]] + chunk["negative_candidates"]
                proc_candidates = []

                for candidate in candidates:
                    target = tokenizer(candidate)
                    if len(target) > max_target_length:
                        target = target[:max_target_length]
                    proc_candidates.append(target)

            else:
                proc_candidates = None

            query2video[chunk_id] = match_id
            example['vid'] = match_id
            id2len[chunk_id] = len(input_ids)
            example['input_ids'] = input_ids
            example['candidates'] = proc_candidates
            # we assume that the gold caption is always in position 0
            example['target_index'] = 0
            example['qid'] = str(chunk_id)

            db[str(chunk_id)] = example

    return id2len, query2video, query_data


def process_tvqa(jsonl, db, tokenizer):
    id2len = {}
    query2video = {}  # not sure if useful
    query_data = []
    for line in tqdm(jsonl, desc='processing TVQA with raw QA text'):
        example = json.loads(line)
        query_data.append(copy.copy(example))
        id_ = example['qid']
        input_ids = [tokenizer(example["q"]), tokenizer(example["a0"]),
                     tokenizer(example["a1"]), tokenizer(example["a2"]),
                     tokenizer(example["a3"]), tokenizer(example["a4"])]
        vid = example['vid_name']
        if 'ts' in example:
            ts = example['ts']
        else:
            ts = None

        if 'answer_idx' in example:
            target = example['answer_idx']
        else:
            target = None

        query2video[id_] = vid
        id2len[id_] = [len(input_ids[0]), len(input_ids[1]), len(input_ids[2]),
                       len(input_ids[3]), len(input_ids[4]), len(input_ids[5])]
        example['input_ids'] = input_ids
        example['vid'] = vid
        example['ts'] = ts
        example['target'] = target
        example['qid'] = str(id_)
        db[str(id_)] = example
    return id2len, query2video, query_data


def main(opts):
    if not exists(opts.output):
        os.makedirs(opts.output)
    else:
        raise ValueError('Found existing DB. Please explicitly remove '
                         'for re-processing')
    meta = vars(opts)
    meta['tokenizer'] = opts.toker
    toker = RobertaTokenizer.from_pretrained(
        opts.toker)
    tokenizer = roberta_tokenize(toker)
    meta['BOS'] = toker.convert_tokens_to_ids(['<s>'])[0]
    meta['EOS'] = toker.convert_tokens_to_ids(['</s>'])[0]
    meta['SEP'] = toker.convert_tokens_to_ids(['</s>'])[0]
    meta['CLS'] = toker.convert_tokens_to_ids(['<s>'])[0]
    meta['PAD'] = toker.convert_tokens_to_ids(['<pad>'])[0]
    meta['MASK'] = toker.convert_tokens_to_ids(['<mask>'])[0]
    meta['UNK'] = toker.convert_tokens_to_ids(['<unk>'])[0]

    meta['v_range'] = (toker.convert_tokens_to_ids(['.'])[0],
                       toker.convert_tokens_to_ids(['<|endoftext|>'])[0] + 1)
    save_json(vars(opts), f'{opts.output}/meta.json', save_pretty=True)

    open_db = curry(open_lmdb, opts.output, readonly=False)
    with open_db() as db:
        with open(opts.annotation, "r") as ann:
            if opts.task == "tvr":
                id2lens, query2video, query_data = process_tvr(
                    ann, db, tokenizer)
            elif opts.task == "tvqa":
                id2lens, query2video, query_data = process_tvqa(
                    ann, db, tokenizer)
            elif opts.task == "football_generative":
                id2lens, query2video, query_data = process_football_generative(
                    ann, db, tokenizer, meta['max_target_length'])
            elif opts.task == "football_retrieval":
                id2lens, query2video, query_data = process_football_retrieval(
                    ann, db, tokenizer, meta['max_target_length'])
            else:
                raise NotImplementedError(
                    f"prepro for {opts.task} not implemented")

    save_json(id2lens, f'{opts.output}/id2len.json')
    save_json(query2video, f'{opts.output}/query2video.json')
    save_jsonl(query_data, f'{opts.output}/query_data.jsonl')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--annotation', required=True,
                        help='annotation JSON')
    parser.add_argument('--output', required=True,
                        help='output dir of DB')
    parser.add_argument('--toker', default='roberta-base',
                        help='which RoBerTa tokenizer to used')
    parser.add_argument('--task', default='tvr',
                        choices=["tvr", "tvqa", "football_generative", "football_retrieval"],
                        help='which RoBerTa tokenizer to used')
    parser.add_argument('--max_target_length',
                        help="Defines the maximum sequence length for generation tasks", default=20)
    args = parser.parse_args()
    main(args)
