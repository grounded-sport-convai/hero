import os
import subprocess
from argparse import ArgumentParser

import torch
from tqdm import tqdm


def main(args):
    print(f"Running NLG evaluation for models in folder '{args.models_dir}'")
    failed = set()

    for model_dir in tqdm(os.listdir(args.models_dir)):
        eval_filename = os.path.basename(args.data)

        model_package = os.path.join(args.models_dir, model_dir, "model.tar.gz")

        if not os.path.exists(model_package):
            print(f"Skipping {model_package} because the file doesn't exist...")
            continue

        output_file = os.path.join(args.models_dir, model_dir, f"{eval_filename}.output")
        annotations_file = os.path.join(args.models_dir, model_dir, f"{eval_filename}.annotations.tsv")

        try:
            func_args = [
                "allennlp",
                "goal-predict",
                model_package,
                args.data,
                "--use-dataset-reader",
                "--include-package",
                "hero",
                "--nlg-metrics",
            ]
            func_args.extend(args.nlg_metrics)
            func_args.extend([
                "--output-file",
                output_file,
                "--annotations-file",
                annotations_file
            ])

            if args.batch_size:
                func_args.extend([
                    "--batch-size",
                    str(args.batch_size)
                ])

            if args.cuda_device != -1 and torch.cuda.is_available():
                func_args.extend([
                    "--cuda-device",
                    str(args.cuda_device)
                ])

            if args.silent:
                func_args.extend([
                    "--silent"
                ])

            subprocess.run(
                func_args,
                check=True
            )
        except subprocess.SubprocessError as ex:
            print(f"Something wrong happened while running evaluation: {ex}")
            failed.add(model_package)

    if failed:
        print(f"The script wasn't unable to evaluate the following models:")
        for f in failed:
            print(f"- {f}")


if __name__ == "__main__":
    parser = ArgumentParser()

    parser.add_argument("--models_dir", required=True, type=str,
                        help="Directory containing all the models that you want to evaluate")
    parser.add_argument("--data", default="storage/football_data/splits/val.jsonl", type=str,
                        help="Dataset annotations filepath")
    parser.add_argument("--batch_size", type=int, default=32)
    parser.add_argument("--cuda-device", type=int, default=-1)
    parser.add_argument("--silent",  action='store_true')
    parser.add_argument("--nlg_metrics", nargs="+", default=["bertscore", "meteor", "sacrebleu", "rouge"])

    args = parser.parse_args()
    main(args)
