#/bin/bash

MODEL_DIR=$1

allennlp train config/goal/seqenc-frame-reordering.jsonnet -s $MODEL_DIR/frame_reordering/gru_bs_32_lr_1e-3 --include-package hero --override "{'data_loader': {'batch_size': 32}}";
allennlp train config/goal/seqenc-frame-reordering.jsonnet -s $MODEL_DIR/frame_reordering/gru_bs_64_lr_1e-3 --include-package hero --override "{'data_loader': {'batch_size': 64}}";

allennlp train config/goal/seqenc-frame-reordering.jsonnet -s $MODEL_DIR/frame_reordering/lstm_bs_32_lr_1e-3 --include-package hero --override "{'data_loader': {'batch_size': 32}, 'model': {'seq_encoder':{'type': 'lstm'}}}";
allennlp train config/goal/seqenc-frame-reordering.jsonnet -s $MODEL_DIR/frame_reordering/lstm_bs_64_lr_1e-3 --include-package hero --override "{'data_loader': {'batch_size': 64}, 'model': {'seq_encoder':{'type': 'lstm'}}}";

allennlp train config/goal/self-frame-reordering.jsonnet -s $MODEL_DIR/frame_reordering/transformer_bs_32_lr_5e-5 --include-package hero --override "{'trainer': {'optimizer': {'lr': 5e-5}}}";
allennlp train config/goal/self-frame-reordering.jsonnet -s $MODEL_DIR/frame_reordering/transformer_bs_32_lr_2e-5 --include-package hero --override "{'trainer': {'optimizer': {'lr': 2e-5}}}";
allennlp train config/goal/self-frame-reordering.jsonnet -s $MODEL_DIR/frame_reordering/transformer_bs_32_lr_1e-4 --include-package hero --override "{'trainer': {'optimizer': {'lr': 1e-4}}}";
allennlp train config/goal/self-frame-reordering.jsonnet -s $MODEL_DIR/frame_reordering/transformer_bs_32_lr_3e-4 --include-package hero --override "{'trainer': {'optimizer': {'lr': 3e-4}}}";

allennlp train config/goal/self-frame-reordering.jsonnet -s $MODEL_DIR/frame_reordering/transformer_sinu_bs_32_lr_5e-5 --include-package hero --override "{'trainer': {'optimizer': {'lr': 5e-5}}, 'model': {'seq_encoder':{'positional_encoding': 'sinusoidal'}}}";
allennlp train config/goal/self-frame-reordering.jsonnet -s $MODEL_DIR/frame_reordering/transformer_sinu_bs_32_lr_2e-5 --include-package hero --override "{'trainer': {'optimizer': {'lr': 2e-5}}, 'model': {'seq_encoder':{'positional_encoding': 'sinusoidal'}}}";
allennlp train config/goal/self-frame-reordering.jsonnet -s $MODEL_DIR/frame_reordering/transformer_sinu_bs_32_lr_1e-4 --include-package hero --override "{'trainer': {'optimizer': {'lr': 1e-4}}, 'model': {'seq_encoder':{'positional_encoding': 'sinusoidal'}}}";
allennlp train config/goal/self-frame-reordering.jsonnet -s $MODEL_DIR/frame_reordering/transformer_sinu_bs_32_lr_3e-4 --include-package hero --override "{'trainer': {'optimizer': {'lr': 3e-4}}, 'model': {'seq_encoder':{'positional_encoding': 'sinusoidal'}}}";