#!/bin/bash

# Create required storage folders
mkdir -p storage/football_data/;
mkdir -p storage/models/;

# Download pretrained HERO model
wget "https://www.dropbox.com/s/7ysfh7tbb12vtfk/hero_pretrained.zip?dl=1" -O storage/models/hero-pretrained.zip;
unzip storage/models/hero-pretrained.zip -d storage/models/

# Download data
wget https://www.dropbox.com/sh/vc7mbdhnt4bt8vc/AACcvA_7Ly-QcRE4lv40o3zAa?dl=1 -O storage/data.zip;
unzip storage/data.zip -d storage/football_data/;
rm storage/data.zip

unzip storage/football_data/video_db.zip -d storage/football_data;
unzip storage/football_data/txt_db.zip -d storage/football_data;




