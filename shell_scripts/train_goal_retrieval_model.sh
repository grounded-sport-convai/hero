#!/usr/bin/env bash

source activate goal

# Common paths
export CURRENT_DIR=${PWD}
export PROJECT_DIR="$(dirname "$CURRENT_DIR")"
cd $PROJECT_DIR

export MODEL_DIR=${PROJECT_DIR}/models

allennlp train config/goal/hero-random-retrieval.jsonnet -s $MODEL_DIR/retrieval/random/hero_flat_bs_32_lr_5e-6 --include-package hero --override "{'trainer': {'optimizer': {'lr': 5e-6}}}";


