#!/usr/bin/env bash

#source activate goal

eval "$(conda shell.bash hook)"
conda init
conda activate goal

# Common paths
export CURRENT_DIR=${PWD}
export PARENT_DIR="$(dirname "$CURRENT_DIR")"
cd $PARENT_DIR

PYTHONPATH=. python tests/test_goal_retrieval_model.py