from typing import List, Any, Dict

from allennlp.data.dataloader import TensorDict
from allennlp.training import GradientDescentTrainer, TensorboardWriter
from allennlp.training.trainer import BatchCallback
from overrides import overrides
from tensorboardX import SummaryWriter
import pandas as pd


@BatchCallback.register("log-decoder-outputs")
class LogDecoderOutputsCallback(BatchCallback):
    def __init__(self, history_key="history", target_key="target", match_id_key="match_id",
                 prediction_key="predicted_captions"):
        self.match_id_key = match_id_key
        self.prediction_key = prediction_key
        self.target_key = target_key
        self.history_key = history_key

    @overrides
    def __call__(
            self,
            trainer: GradientDescentTrainer,
            batch_inputs: List[List[TensorDict]],
            batch_outputs: List[Dict[str, Any]],
            batch_metrics: Dict[str, Any],
            epoch: int,
            batch_number: int,
            is_training: bool,
            is_master: bool
    ) -> None:
        """
        This is called every batch. We will be plotting only at validation time to avoid decreasing performance at
        training time. We will be using a specific key to access the `batch_outputs` that is set in the constructor
        of the class
        """

        if not is_training and is_master:
            tb_writer: TensorboardWriter = trainer._tensorboard
            val_logger: SummaryWriter = tb_writer._validation_log
            text_logs = {
                "history": [],
                "match_id": [],
                "target": [],
                "prediction": []
            }

            for inputs in batch_inputs:
                for meta in inputs["metadata"]:
                    text_logs["history"].append(meta[self.history_key])
                    text_logs["match_id"].append(meta[self.match_id_key])
                    text_logs["target"].append(meta[self.target_key])

            for outputs in batch_outputs:
                for caption in outputs[self.prediction_key]:
                    text_logs["prediction"].append(caption)

            text_logs = pd.DataFrame(data=text_logs, columns=["match_id", "history", "prediction", "target"])
            val_logger.add_text("predicted_captions", text_logs.to_markdown(), tb_writer.get_batch_num_total())
