import jsonlines


def read_dataset(file_path):
    with jsonlines.open(file_path) as in_file:
        for match in in_file:
            yield match
