import copy
import random
from datetime import datetime
from typing import Iterable, Dict, Optional, Any

import jsonlines
import numpy as np
import torch
from allennlp.data import DatasetReader, Instance, TokenIndexer, Tokenizer
from allennlp.data.fields import MetadataField, TextField, ListField, IndexField, ArrayField, LabelField
from allennlp.data.tokenizers.sentence_splitter import SpacySentenceSplitter

from hero.data.utils import VideoFeatLmdb, SubTokLmdb, VideoFeatSubTokDataset


class GoalDatasetReader(DatasetReader):
    def __init__(self,
                 video_db: Dict[str, Any],
                 txt_db_path: str,
                 token_indexers: Dict[str, TokenIndexer] = None,
                 tokenizer: Optional[Tokenizer] = None,
                 lazy: bool = False):
        super().__init__(lazy=lazy)
        self._tokenizer = tokenizer
        self._token_indexers = token_indexers
        self.video_db_config = video_db
        self._txt_db = SubTokLmdb(txt_db_path, self.video_db_config.get("max_clip_len", -1))
        self._video_db = VideoFeatLmdb(
            img_dir=self.video_db_config["path"],
            feat_version=self.video_db_config.get("feat_version", "resnet_slowfast"),
            frame_interval=self.video_db_config.get("frame_interval", 1.5),
            compress=self.video_db_config.get("compress", False),
            max_clip_len=self.video_db_config.get("max_clip_len", -1)
        )


@DatasetReader.register("goal-retrieval")
class GoalResponseRetrievalReader(GoalDatasetReader):
    def __init__(self,
                 video_db: Dict[str, Any],
                 txt_db_path: str,
                 token_indexers: Dict[str, TokenIndexer] = None,
                 tokenizer: Optional[Tokenizer] = None,
                 num_neg_candidates: int = 99,
                 max_txt_length: int = 50,
                 max_video_length: int = 100,
                 lazy: bool = False):
        super().__init__(video_db, txt_db_path, token_indexers, tokenizer, lazy)
        self._txt_video_db = VideoFeatSubTokDataset(self._txt_db, self._video_db, max_txt_length)
        self.num_candidates = num_neg_candidates + 1
        self.max_video_length = max_video_length
        self.max_txt_length = max_txt_length

    def _read(self, file_path: str) -> Iterable[Instance]:
        with jsonlines.open(file_path) as in_file:
            for match_data in in_file:
                match_id = match_data['match_id']
                (
                    frame_level_input_ids,  # num_subs list[tensor(sep,w0,w1,...)]
                    frame_level_v_feats,  # num_subs list[tensor(#sub_frames, d)]
                    frame_level_attn_masks,  # num_subs list[L_sub + #sub_frames]
                    clip_level_v_feats,  # tensor(num_frames, d)
                    clip_level_attn_masks,  # #frames list[1]
                    num_subs,
                    sub2frames  # num_subs, [(sub_ix, [frame_ix]) ]
                ) = self._txt_video_db[match_id]
                for chunk_idx, (v_feats, attn_masks) in enumerate(zip(frame_level_v_feats,
                                                                      frame_level_attn_masks)):
                    chunk = match_data["chunks"][chunk_idx]
                    if chunk_idx >= 1:
                        history = match_data["chunks"][chunk_idx - 1]["caption"]
                    else:
                        history = "."

                    yield self.text_to_instance(
                        match_id,
                        chunk_idx,
                        history,
                        chunk,
                        v_feats,
                        attn_masks,
                        clip_level_v_feats,
                        clip_level_attn_masks,
                        num_subs,
                        sub2frames
                    )

    def text_to_instance(self,
                         match_id,
                         chunk_idx,
                         history,
                         chunk,
                         v_feats,
                         attn_masks,
                         c_v_feats,
                         c_attn_masks,
                         num_subs,
                         sub2frames) -> Instance:
        example_id = f"{match_id}_s{chunk['start']}_e{chunk['end']}"

        metadata = MetadataField({
            "example_id": example_id,
            "match_id": match_id,
            "chunk_idx": chunk_idx,
            "history": history,
            "caption": chunk["caption"],
            "negative_candidates": chunk["negative_candidates"],
            "sub_idx2frame_idx": sub2frames,
            "num_subs": num_subs
        })

        history_tokens = self._tokenizer.tokenize(history)

        if 'caption' in chunk:
            target_index = random.randint(0, self.num_candidates - 1)
            candidates = [None] * self.num_candidates

            for i in range(self.num_candidates):
                if i == target_index:
                    candidates[i] = chunk["caption"]
                elif i < target_index:
                    candidates[i] = chunk["negative_candidates"][i]
                else:
                    candidates[i] = chunk["negative_candidates"][i - 1]

            proc_candidates = []
            candidate_input_masks = []
            for alternative in candidates:
                alternative_tokens = self._tokenizer.tokenize(alternative)
                if len(alternative_tokens) > self.max_txt_length:
                    alternative_tokens = alternative_tokens[-self.max_txt_length:]

                if len(history_tokens) > self.max_txt_length:
                    history_tokens = history_tokens[-self.max_txt_length:]

                curr_tokens = self._tokenizer.add_special_tokens(history_tokens, alternative_tokens)
                proc_candidates.append(
                    TextField(
                        curr_tokens,
                        self._token_indexers
                    )
                )

                candidate_input_masks.append(ArrayField(np.ones(len(curr_tokens) + v_feats.shape[0])))

            candidate_fields = ListField(proc_candidates)
            target_index_field = IndexField(target_index, candidate_fields)
            candidate_input_masks = ListField(candidate_input_masks)

        else:
            candidate_fields = None
            candidates = []
            target_index_field = None
            candidate_input_masks = None

        # frame_level_input_ids,  # num_subs list[tensor(sep,w0,w1,...)]
        # frame_level_v_feats,  # num_subs list[tensor(#sub_frames, d)]
        # frame_level_attn_masks,  # num_subs list[L_sub + #sub_frames]
        # clip_level_v_feats,  # tensor(num_frames, d)
        # clip_level_attn_masks,  # #frames list[1]
        # num_subs,
        # sub2frames
        example = {
            'metadata': metadata,
            'candidates': candidate_fields,
            'target_index': target_index_field,  # we assume that we have integers already
            'f_v_feats': ArrayField(v_feats.numpy()),
            'f_v_masks': ArrayField(np.ones(v_feats.shape[0])),
            'c_v_feats': ArrayField(c_v_feats.numpy()),
            'c_attn_masks': ArrayField(c_attn_masks.numpy()),
            'f_attn_masks': candidate_input_masks
        }

        return Instance(example)


def windowed(seq, n):
    for i in range(0, seq, n):
        yield i, i + n


@DatasetReader.register("goal-frame-reordering")
class GoalFrameReorderingReader(GoalDatasetReader):
    def __init__(self,
                 video_db: Dict[str, Any],
                 txt_db_path: str,
                 token_indexers: Dict[str, TokenIndexer] = None,
                 tokenizer: Optional[Tokenizer] = None,
                 max_video_length: int = 100,
                 max_txt_length: int = 50,
                 sampling_probability: float = 0.15,
                 random_seed: int = 12345,
                 lazy: bool = False):
        super().__init__(video_db, txt_db_path, token_indexers, tokenizer, lazy)
        self._txt_video_db = VideoFeatSubTokDataset(self._txt_db, self._video_db)
        self.max_video_length = max_video_length
        self.sampling_probability = sampling_probability
        self.max_txt_length = max_txt_length
        self.random_seed = random_seed

    def _read(self, file_path: str) -> Iterable[Instance]:
        # we fix the random seed to make sure that we sample always the same chunks
        random.seed(self.random_seed)
        np.random.seed(self.random_seed)
        torch.manual_seed(self.random_seed)

        with jsonlines.open(file_path) as in_file:
            for match_data in in_file:
                match_id = match_data['match_id']
                (
                    _,  # num_subs list[tensor(sep,w0,w1,...)]
                    frame_level_v_feats,  # num_subs list[tensor(#sub_frames, d)]
                    frame_level_attn_masks,  # num_subs list[L_sub + #sub_frames]
                    clip_level_v_feats,  # tensor(num_frames, d)
                    clip_level_attn_masks,  # #frames list[1]
                    num_subs,
                    sub2frames  # num_subs, [(sub_ix, [frame_ix]) ]
                ) = self._txt_video_db[match_id]
                num_chunks = len(match_data["chunks"])
                for chunk_idx, chunk in enumerate(match_data["chunks"]):
                    instance = self.text_to_instance(
                        match_id,
                        chunk_idx,
                        chunk,
                        frame_level_v_feats[chunk_idx]
                    )

                    if instance is not None:
                        yield instance

    def _random_reorder(self, pos_ids):
        """
        random reorder frame positions
        """
        selected_pos = []
        target_pos = []
        for i, pos_id in enumerate(pos_ids):
            prob = random.random()
            # mask token with 15% probability
            if prob < self.sampling_probability:
                selected_pos.append(i)
                target_pos.append(pos_id)

        target_pos_shuffled = copy.deepcopy(target_pos)
        random.shuffle(target_pos_shuffled)
        output_order = copy.deepcopy(pos_ids)
        output_target = []
        for i, pos in enumerate(selected_pos):
            output_order[pos] = target_pos_shuffled[i]
            output_target.append(pos)

        return output_order, output_target, selected_pos

    def text_to_instance(self,
                         match_id,
                         chunk_idx,
                         chunk,
                         v_feats
                         ) -> Instance:
        example_id = f"{match_id}_s{chunk['start']}_e{chunk['end']}"

        metadata = MetadataField({
            "example_id": example_id,
            "match_id": match_id,
            "caption": chunk_idx
        })

        if v_feats.shape[0] > self.max_video_length:
            v_feats = v_feats[:self.max_video_length]

        pos_ids = list(range(0, v_feats.shape[0]))

        output_order, output_target, target_pos = self._random_reorder(pos_ids)

        if not output_target:
            return None

        shuffled_v_feats = v_feats[output_order, :]
        caption_tokens = self._tokenizer.tokenize(chunk["caption"])

        if len(caption_tokens) > self.max_txt_length:
            caption_tokens = caption_tokens[-self.max_txt_length:]

        f_attn_masks = ArrayField(np.ones(len(caption_tokens) + v_feats.shape[0]))
        caption = TextField(caption_tokens, self._token_indexers)

        captions_vfeats = ArrayField(shuffled_v_feats)
        output_target = ArrayField(np.array(output_target), padding_value=-100)
        attn_mask = ArrayField(np.ones(shuffled_v_feats.shape[0]))
        target_pos = ArrayField(np.array(target_pos))

        instance = {
            "caption": caption,
            "f_v_feats": captions_vfeats,
            "f_v_masks": attn_mask,
            "f_attn_masks": f_attn_masks,
            "metadata": metadata,
            "target_orders": output_target,
            "target_pos": target_pos
        }

        return Instance(instance)


def get_min_seconds(time_str: str):
    dt = datetime.strptime(time_str, "%H:%M:%S,%f")
    mins = dt.hour * 60 + dt.minute
    secs = dt.second
    return (mins, secs)


PAUSE_TOKEN = "<PAUSE>"


@DatasetReader.register("goal-response-generation")
class GoalRespGenerationReader(GoalDatasetReader):
    def __init__(self,
                 video_db: Dict[str, Any],
                 txt_db_path: str,
                 token_indexers: Dict[str, TokenIndexer] = None,
                 tokenizer: Optional[Tokenizer] = None,
                 max_history_length: int = 50,
                 max_video_length: int = 100,
                 max_target_length: int = 50,
                 window_length: int = 5,
                 lazy: bool = False):
        super().__init__(video_db, txt_db_path, token_indexers, tokenizer, lazy)
        self._txt_video_db = VideoFeatSubTokDataset(self._txt_db, self._video_db, max_history_length)
        self.max_video_length = max_video_length
        self.max_txt_length = max_history_length
        self.max_target_length = max_target_length
        self.sentence_splitter = SpacySentenceSplitter()
        self.window_length = window_length
        # We define <PAUSE> as an extra token for defining the final part of the generated caption
        # self._tokenizer.tokenizer.add_special_tokens({"additional_special_tokens": [PAUSE_TOKEN]})

    def _build_history(self, history_list):
        history_tokens = []

        for i, h in enumerate(history_list):
            if i > 0:
                history_tokens.extend(self._tokenizer.sequence_pair_mid_tokens)
            history_tokens.extend(self._tokenizer.tokenize(h))

        return " ".join(history_list), history_tokens

    def _read(self, file_path: str) -> Iterable[Instance]:
        with jsonlines.open(file_path) as in_file:
            for match_data in in_file:
                match_id = match_data['match_id']
                match_url = match_data["URL"]
                (
                    frame_level_input_ids,  # num_subs list[tensor(sep,w0,w1,...)]
                    frame_level_v_feats,  # num_subs list[tensor(#sub_frames, d)]
                    frame_level_attn_masks,  # num_subs list[L_sub + #sub_frames]
                    clip_level_v_feats,  # tensor(num_frames, d)
                    clip_level_attn_masks,  # #frames list[1]
                    num_subs,
                    sub2frames  # num_subs, [(sub_ix, [frame_ix]) ]
                ) = self._txt_video_db[match_id]

                chunk_sentences = self.sentence_splitter.batch_split_sentences(
                    [chunk["caption"] for chunk in match_data["chunks"]]
                )

                num_chunks = len(match_data["chunks"])
                full_history = ["."]
                # history_tokens = self._tokenizer.tokenize(history[0])

                for chunk_idx in range(num_chunks):
                    v_feats = frame_level_v_feats[chunk_idx]
                    attn_masks = frame_level_attn_masks[chunk_idx]
                    gen_sentences = chunk_sentences[chunk_idx]

                    # we go through all the sentences generated from the current chunk
                    for sent_idx, target_text in enumerate(gen_sentences):
                        history_str, history_tokens = self._build_history(full_history[-self.window_length:])

                        yield self.text_to_instance(
                            match_id,
                            match_url,
                            chunk_idx,
                            match_data["chunks"][chunk_idx],
                            history_str,
                            history_tokens,
                            target_text,
                            v_feats,
                            attn_masks,
                            clip_level_v_feats,
                            clip_level_attn_masks,
                            num_subs,
                            sub2frames
                        )

                        full_history.append(target_text)

    def text_to_instance(self,
                         match_id,
                         match_url,
                         chunk_idx,
                         chunk,
                         history,
                         history_tokens,
                         target_text,
                         v_feats,
                         attn_masks,
                         c_v_feats,
                         c_attn_masks,
                         num_subs,
                         sub2frames) -> Instance:
        example_id = f"{match_id}_s{chunk['start']}_e{chunk['end']}"
        st = get_min_seconds(chunk['start'])
        et = get_min_seconds(chunk['end'])
        example_url = f"{match_url}#t={st[0]}m{st[1]}s&e={et[0]}m{et[1]}s"

        target_tokens = self._tokenizer.tokenize(target_text)

        if len(history_tokens) > self.max_txt_length:
            history_tokens = history_tokens[-self.max_txt_length:]

        history_tokens = self._tokenizer.add_special_tokens(history_tokens)

        if len(target_tokens) > self.max_target_length:
            target_tokens = target_tokens[-self.max_target_length:]

        target_tokens = self._tokenizer.add_special_tokens(target_tokens)

        if v_feats.shape[0] > self.max_video_length:
            v_feats = v_feats[-self.max_video_length:]

        if c_v_feats.shape[0] > self.max_video_length:
            c_v_feats = c_v_feats[-self.max_video_length:]
            c_attn_masks = c_attn_masks[-self.max_video_length:]

        history_field = TextField(history_tokens, self._token_indexers)
        target = TextField(target_tokens, self._token_indexers)

        f_attn_masks = ArrayField(np.ones(len(history_tokens) + v_feats.shape[0]))

        metadata = MetadataField({
            "example_id": example_id,
            "example_url": example_url,
            "match_id": match_id,
            "chunk_idx": chunk_idx,
            "target": target_text,
            "history": history
        })

        example = {
            'metadata': metadata,
            'f_attn_masks': f_attn_masks,
            'f_v_feats': ArrayField(v_feats.numpy()),
            'f_v_masks': ArrayField(np.ones(v_feats.shape[0])),
            'c_v_feats': ArrayField(c_v_feats.numpy()),
            'c_attn_masks': ArrayField(c_attn_masks.numpy()),
            'history': history_field,
            'targets': target
        }

        return Instance(example)


@DatasetReader.register("goal-moment-retrieval")
class GoalMomentRetrievalReader(GoalDatasetReader):
    def __init__(self,
                 video_db: Dict[str, Any],
                 txt_db_path: str,
                 token_indexers: Dict[str, TokenIndexer] = None,
                 tokenizer: Optional[Tokenizer] = None,
                 max_txt_length: int = 100,
                 max_video_length: int = 100,
                 lazy: bool = False):
        super().__init__(video_db, txt_db_path, token_indexers, tokenizer, lazy)
        self._txt_video_db = VideoFeatSubTokDataset(self._txt_db, self._video_db, max_txt_length)
        self.max_video_length = max_video_length
        self.max_txt_length = max_txt_length

    def _read(self, file_path: str) -> Iterable[Instance]:
        with jsonlines.open(file_path) as in_file:
            for match_data in in_file:
                match_id = match_data['match_id']
                (
                    frame_level_input_ids,  # num_subs list[tensor(sep,w0,w1,...)]
                    frame_level_v_feats,  # num_subs list[tensor(#sub_frames, d)]
                    frame_level_attn_masks,  # num_subs list[L_sub + #sub_frames]
                    clip_level_v_feats,  # tensor(num_frames, d)
                    clip_level_attn_masks,  # #frames list[1]
                    num_subs,
                    sub2frames  # num_subs, [(sub_ix, [frame_ix]) ]
                ) = self._txt_video_db[match_id]
                for chunk_idx, (v_feats, attn_masks) in enumerate(zip(frame_level_v_feats,
                                                                      frame_level_attn_masks)):

                    # consider this as an example only if we have aligned visual features corresponding to it
                    if sub2frames[chunk_idx][1]:
                        chunk = match_data["chunks"][chunk_idx]
                        start_index = sub2frames[chunk_idx][1][0]
                        end_index = sub2frames[chunk_idx][1][-1]

                        if start_index < end_index:
                            yield self.text_to_instance(
                                match_id,
                                chunk_idx,
                                start_index,
                                end_index,
                                chunk,
                                v_feats,
                                attn_masks,
                                clip_level_v_feats,
                                clip_level_attn_masks,
                                num_subs,
                                sub2frames
                            )

    def text_to_instance(self,
                         match_id,
                         chunk_idx,
                         start_index,
                         end_index,
                         chunk,
                         v_feats,
                         attn_masks,
                         c_v_feats,
                         c_attn_masks,
                         num_subs,
                         sub2frames) -> Instance:
        example_id = f"{match_id}_s{chunk['start']}_e{chunk['end']}"

        metadata = MetadataField({
            "example_id": example_id,
            "match_id": match_id,
            "chunk_idx": chunk_idx,
            "caption": chunk["caption"],
            "sub_idx2frame_idx": sub2frames,
            "num_subs": num_subs
        })

        query_tokens = self._tokenizer.tokenize(chunk["caption"])

        if len(query_tokens) > self.max_txt_length:
            query_tokens = query_tokens[-self.max_txt_length:]
        query_field = TextField(query_tokens, self._token_indexers)

        num_vis_features = c_v_feats.shape[0]

        # if the number of visual features is greater than the maximum allowed
        # we have to shift the start/end indexes
        if num_vis_features > self.max_video_length:
            if start_index >= num_vis_features:
                # we take the upper part of the visual features first
                c_v_feats = c_v_feats[-self.max_video_length:]
                c_attn_masks = c_attn_masks[-self.max_video_length:]
                # then we shift the indexes
                start_index = start_index - self.max_video_length
                end_index = end_index - self.max_video_length
            else:
                # indexes are ok. we just truncate the visual features
                c_v_feats = c_v_feats[:self.max_video_length]
                c_attn_masks = c_attn_masks[:self.max_video_length]

        start_index_field = LabelField(start_index, label_namespace="index_labels", skip_indexing=True)
        end_index_field = LabelField(end_index, label_namespace="index_labels", skip_indexing=True)

        input_mask = ArrayField(np.ones(len(query_tokens) + c_v_feats.shape[0]))

        example = {
            'metadata': metadata,
            'query': query_field,
            'start_index': start_index_field,
            'end_index': end_index_field,  # we assume that we have integers already
            'f_v_feats': ArrayField(c_v_feats.numpy()),
            'f_v_masks': ArrayField(c_attn_masks.numpy()),
            'f_attn_masks': input_mask
        }

        return Instance(example)
