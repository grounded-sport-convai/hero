from typing import List

from allennlp.data import BatchSampler, Sampler, DataLoader, PyTorchDataLoader, Batch, Instance
from allennlp.data.dataloader import TensorDict
from torch.utils import data

from hero.data.utils import get_gather_index


def get_gather_index_from_batch(instances, batch: Batch):
    txt_lens = []
    video_lengths = []
    max_vl = 0
    padding_lengths = batch.get_padding_lengths()["f_attn_masks"]
    out_size = padding_lengths.get("list_dimension_0", padding_lengths.get("dimension_0"))

    num_candidates = 0
    txt_field_key = "caption" if "caption" in instances[0] else "query" if "query" in instances[0] else "history"

    for b_idx, inst in enumerate(instances):
        vl = inst.get("f_v_feats").array.shape[0]
        max_vl = max(vl, max_vl)
        candidates = inst.get("candidates")

        if candidates is not None:
            for c in candidates.field_list:
                txt_lens.append(c.sequence_length())
                video_lengths.append(vl)
                num_candidates += 1
        else:
            txt_lens.append(inst.get(txt_field_key).sequence_length())
            video_lengths.append(vl)
            num_candidates += 1

    num_examples = len(txt_lens)

    frame_level_gather_index = get_gather_index(txt_lens, video_lengths, num_examples, max_vl, out_size)

    return frame_level_gather_index


def gather_collate(instances: List[Instance]) -> TensorDict:
    batch = Batch(instances)
    frame_level_gather_index = get_gather_index_from_batch(instances, batch)
    batch_dict = batch.as_tensor_dict(batch.get_padding_lengths())

    batch_dict["f_gather_index"] = frame_level_gather_index
    if "target_index" in batch_dict:
        batch_dict["target_index"] = batch_dict["target_index"].squeeze(-1)

    if "target_orders" in batch_dict:
        target_orders = batch_dict["target_orders"]
        targets_mask = (target_orders != -100).float()
        target_orders[target_orders == -100] = 0

        batch_dict["target_mask"] = targets_mask
        batch_dict["target_orders"] = target_orders.long()

    return batch_dict


@DataLoader.register("gather_dataloader", constructor="from_partial_objects")
class GatherDataLoader(PyTorchDataLoader):
    """
    A registrable version of the pytorch
    [DataLoader](https://pytorch.org/docs/stable/data.html#torch.utils.data.DataLoader).
    Firstly, this class exists is so that we can construct a DataLoader
    from a configuration file and have a different default `collate_fn`.
    You can use this class directly in python code, but it is identical to using
    pytorch dataloader with allennlp's custom collate function:

    ```
    from torch.utils.data import DataLoader

    from allennlp.data import allennlp_collate
    # Construct a dataloader directly for a dataset which contains allennlp
    # Instances which have _already_ been indexed.
    my_loader = DataLoader(dataset, batch_size=32, collate_fn=allennlp_collate)
    ```

    Secondly, this class adds a `batches_per_epoch` parameter which, if given, determines the number
    of batches after which an epoch ends.  If this is `None`, then an epoch is set to be one full pass
    through your data.  You might use this if you have a very large dataset and want more frequent
    checkpoints and evaluations on validation data, for instance.

    In a typical AllenNLP configuration file, the `dataset` parameter does not get an entry under
    the "data_loader", it gets constructed separately.
    """

    def __init__(
            self,
            dataset: data.Dataset,
            batch_size: int = 1,
            shuffle: bool = False,
            sampler: Sampler = None,
            batch_sampler: BatchSampler = None,
            num_workers: int = 0,
            # NOTE: The default for collate_fn is different from the normal `None`.
            # We assume that if you are using this class you are using an
            # allennlp dataset of instances, which would require this.
            collate_fn=gather_collate,
            pin_memory: bool = False,
            drop_last: bool = False,
            timeout: int = 0,
            worker_init_fn=None,
            multiprocessing_context: str = None,
            batches_per_epoch: int = None,
    ):
        super().__init__(
            dataset=dataset,
            batch_size=batch_size,
            shuffle=shuffle,
            sampler=sampler,
            batch_sampler=batch_sampler,
            num_workers=num_workers,
            collate_fn=gather_collate,
            pin_memory=pin_memory,
            drop_last=drop_last,
            timeout=timeout,
            worker_init_fn=worker_init_fn,
            multiprocessing_context=multiprocessing_context,
        )
        self._data_generator = super().__iter__()
        self._batches_per_epoch = batches_per_epoch
