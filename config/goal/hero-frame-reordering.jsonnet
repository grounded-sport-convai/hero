local model_name = "roberta-base";
local model_path = "storage/models/pretrained/hero-tv-ht100.pt";
local train_data = "storage/football_data/random_retrieval/train.jsonl";
local valid_data = "storage/football_data/random_retrieval/val.jsonl";
local test_data = "storage/football_data/random_retrieval/test.jsonl";
local batch_size = 32;
local max_seq_length = 50;
local num_epochs = 10;
local max_video_length = 100;
{
    "dataset_reader" : {
        "type": "goal-frame-reordering",
        "video_db": {
            "path": "storage/football_data/video_db/",
            "feat_version": "resnet_slowfast",
            "frame_interval": 1.5,
            "compress": false
        },
        "txt_db_path": "storage/football_data/txt_db/",
        "sampling_probability": 0.15,
        "max_txt_length": max_seq_length,
        "max_video_length": max_video_length,
        "tokenizer": {
            "type": "pretrained_transformer",
            "model_name": model_name,
            "add_special_tokens": false
        },
        "token_indexers": {
            "tokens": {
                "type": "pretrained_transformer",
                "model_name": model_name
            }
        }
    },
    "train_data_path": train_data,
    "validation_data_path": valid_data,
    "model": {
        "type": "hero-goal-reordering",
        "model_path": model_path,
        "classifier": {
            "num_layers": 2,
            "input_dim": 768,
            "hidden_dims": [768, max_video_length],
            "dropout": [0.1, 0.0],
            "activations": ["relu", "linear"]
        }
    },
    "data_loader": {
        "type": "gather_dataloader",
        "sampler": "random",
        "batch_size": batch_size
    },
    "trainer": {
        "num_epochs": num_epochs,
        "validation_metric": "+accuracy",
        "learning_rate_scheduler": {
          "type": "linear_with_warmup",
          "warmup_steps": 662
        },
        "grad_clipping": 1.0,
        "optimizer": {
          "type": "huggingface_adamw",
          "lr": 5e-05,
          "weight_decay": 0.1
        }
    }
}
