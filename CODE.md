# Code for running GOAL models

## Setup

We have provided [setup.sh](./setup.sh) to streamline the process to download the data and setup the conda environment to reproduce the results reported in the paper 

If there is apex related issue, kindly check this [issue](https://github.com/NVIDIA/apex/issues/988#issuecomment-726343453).

## Code Structure

We provide [shell scripts](/shell_scripts) to test and run basic configuration of our models. Use the following to run as:

```
cd shell_scripts
bash run_test_goal_gen_model.sh
```

This is same as running from the project repository.

```
PYTHONPATH=. python tests/test_goal_gen_model.py
```