"""
The `predict` subcommand allows you to make bulk JSON-to-JSON
or dataset to JSON predictions using a trained model and its
[`Predictor`](../predictors/predictor.md#predictor) wrapper.
"""
import argparse
import csv
import sys
from typing import List, Iterator, Optional, Dict

import datasets
from allennlp.commands.subcommand import Subcommand
from allennlp.common import logging as common_logging
from allennlp.common.checks import check_for_gpu, ConfigurationError
from allennlp.common.file_utils import cached_path
from allennlp.common.util import lazy_groups_of
from allennlp.data import Instance
from allennlp.models.archival import load_archive
from allennlp.predictors.predictor import Predictor, JsonDict
from datasets import Metric
from overrides import overrides
from tqdm import tqdm

"""
Example command
allennlp goal-predict storage/models/hero_flat_bs_32_lr_5e-5/ storage/football_data/splits/val.jsonl --use-dataset-reader --weights-file storage/models/hero_flat_bs_32_lr_5e-5/best.th --include-package hero
"""


@Subcommand.register("goal-predict")
class GOALPredict(Subcommand):
    @overrides
    def add_subparser(self, parser: argparse._SubParsersAction) -> argparse.ArgumentParser:
        description = """Run the specified model against a JSON-lines input file."""
        subparser = parser.add_parser(
            self.name, description=description, help="Use a trained model to make predictions."
        )

        subparser.add_argument(
            "archive_file", type=str, help="the archived model to make predictions with"
        )
        subparser.add_argument("input_file", type=str, help="path to or url of the input file")

        subparser.add_argument("--output-file", type=str, help="path to output file")
        subparser.add_argument(
            "--weights-file", type=str, help="a path that overrides which weights file to use"
        )

        batch_size = subparser.add_mutually_exclusive_group(required=False)
        batch_size.add_argument(
            "--batch-size", type=int, default=1, help="The batch size to use for processing"
        )

        subparser.add_argument(
            "--silent", action="store_true", help="do not print output to stdout"
        )

        cuda_device = subparser.add_mutually_exclusive_group(required=False)
        cuda_device.add_argument(
            "--cuda-device", type=int, default=-1, help="id of GPU to use (if any)"
        )

        subparser.add_argument(
            "--use-dataset-reader",
            action="store_true",
            help="Whether to use the dataset reader of the original model to load Instances. "
                 "The validation dataset reader will be used if it exists, otherwise it will "
                 "fall back to the train dataset reader. This behavior can be overridden "
                 "with the --dataset-reader-choice flag.",
        )

        subparser.add_argument(
            "--dataset-reader-choice",
            type=str,
            choices=["train", "validation"],
            default="validation",
            help="Indicates which model dataset reader to use if the --use-dataset-reader "
                 "flag is set.",
        )

        subparser.add_argument(
            "-o",
            "--overrides",
            type=str,
            default="",
            help=(
                "a json(net) structure used to override the experiment configuration, e.g., "
                "'{\"iterator.batch_size\": 16}'.  Nested parameters can be specified either"
                " with nested dictionaries or with dot syntax."
            ),
        )

        subparser.add_argument(
            "--predictor", type=str, help="optionally specify a specific predictor to use"
        )

        subparser.add_argument(
            "--file-friendly-logging",
            action="store_true",
            default=False,
            help="outputs tqdm status on separate lines and slows tqdm refresh rate",
        )

        subparser.add_argument(
            "--annotations-file",
            type=str,
            help="CSV file where to store the predictions ready to be annotated"
        )

        subparser.add_argument(
            "--nlg-metrics",
            nargs="*",
            default=["bertscore", "sacrebleu", "meteor", "rouge"],
            help="List of metrics to use for NLG evaluation (see https://huggingface.co/metrics)"
        )

        subparser.set_defaults(func=_predict)

        return subparser


def _get_predictor(args: argparse.Namespace) -> Predictor:
    check_for_gpu(args.cuda_device)
    archive = load_archive(
        args.archive_file,
        weights_file=args.weights_file,
        cuda_device=args.cuda_device,
        overrides=args.overrides,
    )

    return Predictor.from_archive(
        archive, args.predictor, dataset_reader_to_load=args.dataset_reader_choice
    )


class _PredictManager:
    def __init__(
            self,
            predictor: Predictor,
            input_file: str,
            output_file: Optional[str],
            annotations_file: Optional[str],
            batch_size: int,
            print_to_console: bool,
            has_dataset_reader: bool,
            nlg_metrics: List[str]
    ) -> None:

        self._predictor = predictor
        self._input_file = input_file
        self._output_file = None if output_file is None else open(output_file, "w")
        self._annotations_file = None if annotations_file is None else open(annotations_file, "w")
        self._batch_size = batch_size
        self._print_to_console = print_to_console
        self._dataset_reader = None if not has_dataset_reader else predictor._dataset_reader
        self._nlg_metrics = nlg_metrics

    def _predict_json(self, batch_data: List[JsonDict]) -> Iterator[Dict]:
        if len(batch_data) == 1:
            results = [self._predictor.predict_json(batch_data[0])]
        else:
            results = self._predictor.predict_batch_json(batch_data)
        for output in results:
            yield output

    def _predict_instances(self, batch_data: List[Instance]) -> Iterator[Dict]:
        if len(batch_data) == 1:
            results = [self._predictor.predict_instance(batch_data[0])]
        else:
            results = self._predictor.predict_batch_instance(batch_data)
        for output in results:
            yield output

    def _maybe_print_to_console_and_file(
            self, index: int, prediction: str, model_input: Instance = None
    ) -> None:
        if self._print_to_console:
            if model_input is not None:
                match_id = model_input.get("metadata").get("match_id")

                print(f"input {index} - Match ID: {match_id}\n\nInput: {str(model_input)}")
                print(f"History: {model_input.get('metadata').get('history')}")

            print("prediction: ", prediction)
        if self._output_file is not None:
            self._output_file.write(prediction)

    def _get_json_data(self) -> Iterator[JsonDict]:
        if self._input_file == "-":
            for line in sys.stdin:
                if not line.isspace():
                    yield self._predictor.load_line(line)
        else:
            input_file = cached_path(self._input_file)
            with open(input_file, "r") as file_input:
                for line in file_input:
                    if not line.isspace():
                        yield self._predictor.load_line(line)

    def _get_instance_data(self) -> Iterator[Instance]:
        if self._input_file == "-":
            raise ConfigurationError("stdin is not an option when using a DatasetReader.")
        elif self._dataset_reader is None:
            raise ConfigurationError("To generate instances directly, pass a DatasetReader.")
        else:
            yield from self._dataset_reader.read(self._input_file)

    def run(self) -> None:
        has_reader = self._dataset_reader is not None
        index = 0
        if self._annotations_file:
            print(f"Writing annotations to file {self._annotations_file}")
            csv_annotation = csv.writer(self._annotations_file, dialect=csv.excel_tab)
            csv_annotation.writerow(["Match ID", "URL", "History", "Generated sentence", "Target sentence"])
        else:
            csv_annotation = None

        if self._nlg_metrics:
            metrics: Dict[str, Metric] = {
                metric_name: datasets.load_metric(metric_name)
                for metric_name in self._nlg_metrics
            }
        else:
            metrics = {}

        if has_reader:
            for batch in tqdm(lazy_groups_of(self._get_instance_data(), self._batch_size)):
                for model_input_instance, result in zip(batch, self._predict_instances(batch)):
                    metadata = model_input_instance.get("metadata")
                    if "target" in metadata and "predicted_captions" in result:
                        if metrics:
                            for metric_name, metric in metrics.items():
                                metric.add(prediction=result["predicted_captions"],
                                           reference=metadata.get("target") if metric_name != "sacrebleu" else [
                                               metadata.get("target")])

                        if csv_annotation:
                            csv_annotation.writerow([
                                metadata.get("match_id"),
                                metadata.get("example_url"),
                                metadata.get("history"),
                                result["predicted_captions"],
                                metadata.get("target")
                            ])

                    result_str = self._predictor.dump_line(result)
                    self._maybe_print_to_console_and_file(index, result_str, model_input_instance)
                    index = index + 1
        else:
            for batch_json in tqdm(lazy_groups_of(self._get_json_data(), self._batch_size)):
                for model_input_json, result in zip(batch_json, self._predict_json(batch_json)):
                    metadata = model_input_json.get("metadata")
                    if "target" in metadata and "predicted_captions" in result:
                        if metrics:
                            for metric_name, metric in metrics:
                                metric.add(prediction=result["predicted_captions"],
                                           reference=metadata.get("target") if metric_name != "sacrebleu" else [
                                               metadata.get("target")])

                        if csv_annotation:
                            csv_annotation.writerow([
                                metadata.get("match_id"),
                                metadata.get("example_url"),
                                metadata.get("history"),
                                result["predicted_captions"],
                                metadata.get("target")
                            ])
                    result_str = self._predictor.dump_line(result)
                    self._maybe_print_to_console_and_file(
                        index, result_str, model_input_json
                    )
                    index = index + 1

        if metrics:
            print("NLG-eval report")
            scores = compute_metrics(metrics)
            metric_names = sorted(scores.keys())
            header = "\t".join(metric_names)
            scores = "\t".join(str(scores[m]) for m in metric_names)
            print(header)
            print(scores)

            if self._output_file is not None:
                self._output_file.write("NLG-eval report")
                self._output_file.write(header)
                self._output_file.write(scores)

        if self._output_file is not None:
            self._output_file.close()

        if self._annotations_file is not None:
            self._annotations_file.close()


def compute_metrics(metrics):
    scores = {}

    for metric_name, metric in metrics.items():
        if metric_name == "rouge":
            comp_result = metric.compute(rouge_types=["rougeL"])
            scores["rougeL"] = comp_result["rougeL"].mid.fmeasure
        elif metric_name == "bertscore":
            comp_result = metric.compute(lang="en")
            scores["bertscore"] = comp_result["f1"].mean().item()
        elif metric_name == "bleurt":
            # comp_result = metric.compute()
            # scores["bleurt"] = statistics.mean(comp_result["scores"])
            pass
        elif metric_name == "meteor":
            comp_result = metric.compute()
            scores["meteor"] = comp_result["meteor"]
        elif metric_name == "sacrebleu":
            comp_result = metric.compute()
            scores["bleu"] = comp_result["score"]

    return scores


def _predict(args: argparse.Namespace) -> None:
    common_logging.FILE_FRIENDLY_LOGGING = args.file_friendly_logging

    predictor = _get_predictor(args)

    if args.silent and not args.output_file:
        print("--silent specified without --output-file.")
        print("Exiting early because no output will be created.")
        sys.exit(0)

    manager = _PredictManager(
        predictor,
        args.input_file,
        args.output_file,
        args.annotations_file,
        args.batch_size,
        not args.silent,
        args.use_dataset_reader,
        args.nlg_metrics
    )
    manager.run()
