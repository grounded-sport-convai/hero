# Going for GOAL: A Resource for Grounded Football Commentaries

This is the official repository for the GOAL dataset. This repository contains the code to
reproduce the experiments reported in the original paper. To make sure that our experiments are
fully reproducible we use [AllenNLP](https://allenai.org/allennlp). In our experiments, we rely on
[HERO](https://arxiv.org/abs/2005.00200), a state-of-the-art pretrained Video+Language model. Most of the scripts reported in this
codebase are reported as-is from the original HERO codebase. If you intend to use the original
scripts and code, please refer to [HERO's GitHub repository](https://github.com/linjieli222/HERO).

![Overview of GOAL](figures/goal_overview.png)

Some code in this repo are copied/modified from opensource implementations made available by
[PyTorch](https://github.com/pytorch/pytorch),
[HuggingFace](https://github.com/huggingface/transformers),
[OpenNMT](https://github.com/OpenNMT/OpenNMT-py),
[Nvidia](https://github.com/NVIDIA/DeepLearningExamples/tree/master/PyTorch),
[TVRetrieval](https://github.com/jayleicn/TVRetrieval),
[TVCaption](https://github.com/jayleicn/TVCaption),
and [UNITER](https://github.com/ChenRocks/UNITER).
The visual frame features are extracted using
[SlowFast](https://github.com/facebookresearch/SlowFast) and ResNet-152. Feature extraction code is
available at
[HERO_Video_Feature_Extractor](https://github.com/linjieli222/HERO_Video_Feature_Extractor).

## Requirements

### Python setup
First clone this repository on your development machine using `git clone` and then `cd <your_workspace>/goal-baselines`. 

We will use Anaconda to set up the environment. Please download it first and then use the following
commands:
```
conda create -n goal python=3.7 -y
conda activate goal
conda install jupyter ninja -y
conda install pytorch==1.7.1 torchvision==0.8.2 torchaudio==0.7.2 -c pytorch
pip install -r requirements.txt

git clone -b 22.04-dev https://github.com/NVIDIA/apex /tmp/apex
pip install -v --disable-pip-version-check --no-cache-dir --global-option="--cpp_ext" --global-option="--cuda_ext" /tmp/apex
```

### Data download

We provide a BASH script to download all the files required for the experiments as well as
additional data that we provide with our benchmark. Please run the following script to complete
this step: 

```bash
sh scripts/download_goal.sh
```

## Training

Once all the dependencies are installed, we can use the AllenNLP CLI commands to train a model.
Below we showcase how to train a model for Moment Retrieval:

```
allennlp train config/goal/hero-moment-retrieval.jsonnet -s storage/models/moment_retrieval/hero_flat_bs_64_lr_5e-5 --include-package hero
```

You can follow a similar approach to train other models whose configurations are reported in the
folder `config/goal`.

## Evaluation

The evaluation for a given task is equally done using AllenNLP. For instance, for the moment
retrieval task we can use the `evaluate` feature in AllenNLP to evaluate a model checkpoint on the
test data:

```
allennlp evaluate <model_archive> storage/splits/test.jsonl
```

### Pretrained models

We release several pretrained models for several downstream tasks in GOAL. You can access them at
the following [Dropbox folder](https://www.dropbox.com/sh/wbs45xpu6vkrle7/AAAreUepKTzFcLPG4Ygvdfuqa?dl=0).